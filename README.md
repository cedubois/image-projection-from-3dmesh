# Generation of Realistic 2D projection of 3D mesh

---

## Usage
`generator = GenView(file_path=fp, verbose=1)` <br>
`generator.generate_n_img(N=25,
                         width=128,
                         sigma_psf=1.5,
                         mean_noise=0.,
                         sigma_noise=0.02,
                         )`
See the docstring for more parameters.
## Example
UVP-like image of a Copepod. See <a href="https://ecotaxa.obs-vlfr.fr/">EcoTaxa</a> for real images.
<img src="./illustration.png" width="600"/>

## Author & License
Copyright UCA/CNRS/Inria
Contributor(s): Cedric Dubois 2022

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".