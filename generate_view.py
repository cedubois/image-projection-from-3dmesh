# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
#
from scipy.ndimage import gaussian_filter
import os
import numpy as np
import meshio
import plotly.graph_objects as go
from pathlib import Path
import io
from PIL import Image
import imageio



def rgb2gray(rgb):
    """
    Input : RGB image (0,255).
    Return a normalized grayscale image.
    :param rgb: rgb image
    :return: grayscale image
    """
    return np.dot(rgb[..., :3], [0.2989 / 255., 0.5870 / 255., 0.1140 / 255.])


def degrade_image(img,
                  sigma_psf=1.,
                  sigma_noise=0.02,
                  mean_noise=0.15):
    """
    Degrade image with gaussian PSF and additive normal noise
    :return: degraded image
    """
    # Gaussian Filter :
    img = gaussian_filter(img, sigma=sigma_psf)
    # Add normal noise :
    img += np.random.normal(loc=mean_noise, scale=sigma_noise, size=img.shape)
    # clip [0,1]
    return np.clip(img, 0., 1.)

class GenView():
    """
    Class for generating realistic (grayscale) 2D projection of 3D mesh object.
    3D backend: plotly
    """

    def __init__(self,
                 file_path,
                 verbose=0):
        """
        Parameters
       ----------
       :param file_path: str, file path to the 3D mesh
       :param verbose: int or bool
        """
        self.fp = file_path
        self.verbose = verbose

        self.mesh = meshio.read(file_path)
        if self.verbose:
            print(f"successfully loaded 3D mesh {self.fp}")

    def generate_projection(self, **kwargs):
        """
        Construct figure
        Parameters
       ----------
        :param kwargs: kwargs for plotly.graph_objects.go.Mesh3d
        """
        x = self.mesh.points[:, 0]
        x -= (x.max() + x.min()) / 2
        y = self.mesh.points[:, 1]
        y -= (y.max() + y.min()) / 2
        z = self.mesh.points[:, 2]
        z -= (z.max() + z.min()) / 2

        cells = self.mesh.cells_dict['triangle']

        i = cells[:, 0]
        j = cells[:, 1]
        k = cells[:, 2]

        lay = {'plot_bgcolor': 'white',
               'paper_bgcolor': 'white',
               'scene_aspectmode': 'data',
               'scene': {
                    'xaxis': dict(visible=False,),
                    'yaxis': dict(visible=False,),
                    'zaxis': dict(visible=False,)
                        }
               }

        self.fig = go.Figure(layout=lay,
                        data=[go.Mesh3d(x=x, y=y, z=z,
                                        i=i, j=j, k=k,
                                        color='black',
                                        **kwargs,)])

    def update_camera_pose(self, theta, phi, r=3):
        """
        Update camera position with spherical coordinates
        Parameters
       ----------
        :param theta: elevation angle
        :param phi: azimuth angle
        :param r: distance to object
        :return:
        """
        x_cam = r * np.sin(theta) * np.cos(phi)
        y_cam = r * np.sin(theta) * np.sin(phi)
        z_cam = r * np.cos(theta)

        camera = dict(
            up=dict(x=1, y=0, z=0),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=x_cam, y=y_cam, z=z_cam)
        )

        self.fig.update_layout(scene_camera=camera)

    def show_fig(self):
        """
        Open dynamic figure in browser
        """
        self.fig.show()

    def screenshot(self, width=1000, n_px=128, format='png'):
        """
        take and save screenshot of the actual pose
        Only square format
        Parameters
       ----------
        :param width: relative width and height, default 1000
        :param n_px: final pixel width and height, default 128
        :param format: format of the image
        :return: None
        """
        img = self.fig.to_image(format=format, width=width, height=width,
                        scale=n_px / width, engine='auto')

        img_data = np.asarray(Image.open(io.BytesIO(img))).copy()

        return img, img_data

    def save_img(self,img, save_path='./', file_name='my_screenshot', format='png'):
        """
        save image
        Parameters
       ----------
        :param save_path: path to save the file (only used if save=True), default './'
        :param file_name: name of the file to save(only used if save=True), default 'my_screenshot'
        :param format: format of the image
        """
        if not os.path.exists(save_path):
            os.mkdir(save_path)

        file = save_path + file_name + '.' + format
        Path(file).write_bytes(img)

    def save_img_from_array(self, img, save_path='./', file_name='my_screenshot', format='png'):
        """
        save image from numpy array
        Parameters
       ----------
        :param save_path: path to save the file (only used if save=True), default './'
        :param file_name: name of the file to save(only used if save=True), default 'my_screenshot'
        :param format: format of the image
        """
        if not os.path.exists(save_path):
            os.mkdir(save_path)

        file = save_path + file_name + '.' + format
        imageio.imwrite(file, img)

    def generate_n_img(self,
                       N,
                       width=128,
                       r=2.75,
                       fixed_scale=False,
                       opacity='uniform',
                       sigma_opacity=0.15,
                       mean_opacity=0.6,
                       sigma_psf=1.5,
                       sigma_noise=0.02,
                       mean_noise=0.2,
                       save_path='./generated_images/',
                       save_clean_img=False,
                       show_fig=False,
                       **kwargs):
        """
        Generate N view of the 3D mesh with random parameters (camera position, opacity object, scale/distance to object)
        Parameters
       ----------
        :param N: Number of view to generate
        :param width: width of the final image
        :param r: scale i.e. distance to object
        :param fixed_scale: to fix distance
        :param opacity: str, float [0,1], opacity of the object. Can be fixed or random ('uniform' or 'normal')
        :param sigma_opacity: float, only used if opacity='normal'
        :param mean_opacity:  float, only used if opacity='normal'
        :param sigma_psf: float, std of the Gaussian PSF, i.e. amount of blurring
        :param sigma_noise: float, std of the Gaussian noise, i.e. spread of the noise
        :param mean_noise: float, mean of the Gaussian noise, i.e. mean amount of noise
        :param save_path: path to save images
        :param save_clean_img: bool, to save clean images i.e. no blur, no noise
        :param show_fig: bool, to open the dynamic figure in a browser
        :param kwargs: kwargs for plotly.graph_objects.go.Mesh3d
        """

        if isinstance(opacity, str):
            opacity_fixed = False
            if opacity=='uniform':
                opacity_range = np.random.random(size=N)
                opacity_start = 1
            elif opacity=='normal':
                opacity_range = np.clip(np.random.normal(size=N, loc=mean_opacity, scale=sigma_opacity), 0, 1)
                opacity_start = 1
            else:
                raise NotImplementedError("Only 'uniform' is implemented for now")
        elif isinstance(opacity, float):
            if opacity > 1 or opacity < 0:
                raise ValueError("opacity kwarg can be str or float in [0,1]")
            else:
                opacity_fixed = True
                opacity_start = opacity
        else:
            raise ValueError("opacity kwarg can be str or float in [0,1]")

        theta = np.random.random(size=N) * 360.  # Theta : azimuth : [0,360]
        # To get uniform distribution on a sphere:
        phi = np.arccos(1 - 2 * np.random.random(size=N)) - np.pi / 2. # Phi : elevation : [0,180]
        if fixed_scale:
            scale = r
        else:
            scale_range = r + np.random.random(size=N) * r  # Max : double scale

        self.generate_projection(opacity=opacity_start, **kwargs)
        if show_fig:
            self.show_fig()

        for k in range(N):
            if not opacity_fixed: # This may slow down
                self.generate_projection(opacity=opacity_range[k], **kwargs)
            if not fixed_scale:
                scale = scale_range[k]

            self.update_camera_pose(theta[k], phi[k], scale)

            img, img_data = self.screenshot(width=1000, n_px=width)

            img_data = rgb2gray(img_data)

            img_d = degrade_image(img_data, sigma_psf=sigma_psf, sigma_noise=sigma_noise, mean_noise=mean_noise, )

            if save_clean_img:
                self.save_img(img, save_path=save_path + "clean/", file_name=f"_pose_{k}_over_{N}", format='png')
                self.save_img_from_array(img_d, save_path=save_path + "degraded/", file_name=f"_pose_{k}_over_{N}",
                                         format='png')
            else:
                self.save_img_from_array(img_data, save_path=save_path, file_name=f"_pose_{k}_over_{N}", format='png')